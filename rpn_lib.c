#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "rpn.h"

#define CLEAR(buff, size) \
    memset(buff, 0, size);

#define DRAII(type, name, size)               \
    type *name = malloc(sizeof(type) * size); \
    CLEAR(name, size);

#define DRAII_EXISTING(type, name, size) \
    name = malloc(sizeof(type) * size);  \
    CLEAR(name, size);

#define NEW(type, name)   \
    malloc(sizeof(type)); \
    CLEAR(name, sizeof(type));

#define POP(buff, index) \
    buff[--(*index)]

#define true 1
#define false 0
#ifndef null
#define null NULL
#endif
#define byte unsigned char

typedef struct node
{
    struct node *lhs, *rhs;
    char c;
} node;

bool walk_side(node *root, node *side, char *buff, int *buffindex, int bufflen, bool forceParens);
bool walk(node *root, char *buff, int *buffindex, int bufflen, bool forceParens);

char operator_lookup[] = {'^', '/', '*', '-', '+'};

bool valid_operand(char c)
{
    if (c >= 'a' && c <= 'z')
        return true;
    return false;
}

bool valid_operator(char c)
{
    for (unsigned int i = 0; i < sizeof(operator_lookup); i++)
        if (operator_lookup[i] == c)
            return true;
    return false;
}

bool valid_open_paren(char c)
{
    return c == '(';
}

bool valid_close_paren(char c)
{
    return c == ')';
}

byte find_op_value(char op)
{
    for (unsigned long i = 0; i < sizeof(operator_lookup); i++)
    {
        if (operator_lookup[i] == op)
            return i;
    }
    return -1;
}

bool op_less_than(char op0, char op1)
{
    if (op1 == '(')
        return true;
    byte op0val = find_op_value(op0);
    byte op1val = find_op_value(op1);
    return op0val - op1val < 0;
}

char peek(char *buff, int *index)
{
    return buff[*index - 1];
}

bool push_char(char *buff, int *index, int len, char val)
{
    if (*index > len)
        return false;
    buff[(*index)++] = val;
    return true;
}

bool push_node(node **buff, int *index, int len, node *val)
{
    if (*index > len)
        return false;
    buff[(*index)++] = val;
    return true;
}

void swap(char *a, char *b)
{
    char c = *a;
    *a = *b;
    *b = c;
}

bool handle_ops(char op, char *buff, int bufflen, int *buffindex, char *tempbuff, int tempbufflen, int *tempbuffindex)
{
    if (*tempbuffindex > 0 && !op_less_than(op, peek(tempbuff, tempbuffindex)))
    {
        push_char(tempbuff, tempbuffindex, tempbufflen, op);
        while (*tempbuffindex > 1 && tempbuff[*tempbuffindex - 2] != '(' && op_less_than(tempbuff[*tempbuffindex - 2], tempbuff[*tempbuffindex - 1]))
        {
            swap(&tempbuff[*tempbuffindex - 1], &tempbuff[*tempbuffindex - 2]);
            push_char(buff, buffindex, bufflen, POP(tempbuff, tempbuffindex));
        }
    }
    else
    {
        push_char(tempbuff, tempbuffindex, tempbufflen, op);
    }
    return true;
}

bool unwind(char *buff, int bufflen, int *buffindex, char *tempbuff, int *tempbuffindex)
{
    char op;
    do
    {
        op = POP(tempbuff, tempbuffindex);
        if (op != '(')
        {
            push_char(buff, buffindex, bufflen, op);
        }
    } while (op != '(' && *tempbuffindex > 0);
    return true;
}

bool infix_to_rpn_impl(const char *infix, int infixmaxlen, int index, char *buff, int bufflen, int *buffindex, char *tempbuff, int tempbufflen, int *tempbuffindex)
{
    if (infix[index] == '\0' || index >= infixmaxlen)
    {
        if (*tempbuffindex > 0)
        {
            bool ret = unwind(buff, bufflen, buffindex, tempbuff, tempbuffindex);
            if (!ret)
                return ret;
        }
        return true;
    }
    char inf = infix[index];
    if (valid_operand(inf))
    {
        if (!push_char(buff, buffindex, bufflen, inf))
            return false;
    }
    else if (valid_operator(inf))
    {
        bool ret = handle_ops(inf, buff, bufflen, buffindex, tempbuff, tempbufflen, tempbuffindex);
        if (!ret)
            return ret;
    }
    else if (valid_open_paren(inf))
    {
        if (!push_char(tempbuff, tempbuffindex, tempbufflen, inf))
            return false;
    }
    else if (valid_close_paren(inf))
    {
        bool ret = unwind(buff, bufflen, buffindex, tempbuff, tempbuffindex);
        if (!ret)
            return ret;
    }
    return infix_to_rpn_impl(infix, infixmaxlen, index + 1, buff, bufflen, buffindex, tempbuff, tempbufflen, tempbuffindex);
}

char *infix_to_rpn(const char *infix, int infixmaxlen, char *buff, int bufflen, char *tempbuff, int tempbufflen)
{
    if (infix == null || buff == null || bufflen < 3)
        return null;
    CLEAR(buff, bufflen);
    bool dynamic = tempbuff == null || tempbufflen == 0;
    if (dynamic)
    {
        tempbufflen = infixmaxlen;
        DRAII_EXISTING(char, tempbuff, tempbufflen);
    }
    else
    {
        CLEAR(tempbuff, tempbufflen);
    }
    int tempbuffindex = 0;
    int buffindex = 0;
    bool ret = infix_to_rpn_impl(infix, infixmaxlen, 0, buff, bufflen, &buffindex, tempbuff, tempbufflen, &tempbuffindex);
    if (dynamic)
    {
        free(tempbuff);
    }
    return ret ? buff : null;
}

bool walk_side(node *root, node *side, char *buff, int *buffindex, int bufflen, bool forceParens)
{
    bool paren = false;
    if (valid_operator(root->c) && valid_operator(side->c) && (op_less_than(root->c, side->c) || forceParens))
    {
        paren = true;
        push_char(buff, buffindex, bufflen, '(');
    }
    bool ret = walk(side, buff, buffindex, bufflen, forceParens);
    if (!ret)
        return ret;
    if (paren)
    {
        push_char(buff, buffindex, bufflen, ')');
    }
    return true;
}

bool walk(node *root, char *buff, int *buffindex, int bufflen, bool forceParens)
{
    if (root->lhs != null)
    {
        bool ret = walk_side(root, root->lhs, buff, buffindex, bufflen, forceParens);
        if (!ret)
            return ret;
    }
    if (!push_char(buff, buffindex, bufflen, root->c))
        return false;
    if (root->rhs != null)
    {
        bool ret = walk_side(root, root->rhs, buff, buffindex, bufflen, forceParens);
        if (!ret)
            return ret;
    }
    free(root);
    return true;
}

node **rpn_to_infix_impl(const char *rpn, int rpnmaxlen, void *tempbuff, int tempbufflen)
{
    node **stack = tempbuff;
    int buffindex = 0, bufflen = tempbufflen / sizeof(void *);
    for (int i = 0; i < rpnmaxlen && rpn[i] != '\0'; i++)
    {
        char c = rpn[i];
        node *n = NEW(node, n);
        n->c = c;
        if (valid_operand(c))
        {
            push_node(stack, &buffindex, bufflen, n);
        }
        else if (valid_operator(c))
        {
            node *rhs = POP(stack, &buffindex);
            node *lhs = POP(stack, &buffindex);
            n->rhs = rhs;
            n->lhs = lhs;
            push_node(stack, &buffindex, bufflen, n);
        }
    }
    if (stack[0]->rhs == null || stack[0]->lhs == null || valid_operand(stack[0]->c)) //if there's no operators, then it's invalid
        return null;
    return stack;
}

char *rpn_to_infix(const char *rpn, int rpnmaxlen, char *buff, int bufflen, void *tempbuff, int tempbufflen, int forceParens)
{
    bool dynamic = tempbuff == null || tempbufflen / sizeof(void *) < 1; //FIXME: handling of the dynamic mem is a little odd
    if (dynamic)
    {
        tempbufflen = rpnmaxlen * sizeof(void *);
        DRAII_EXISTING(char, tempbuff, tempbufflen);
    }
    else
    {
        CLEAR(tempbuff, tempbufflen);
    }
    node **stack = rpn_to_infix_impl(rpn, rpnmaxlen, tempbuff, tempbufflen);
    if (stack != null)
    {
        int buffindex = 0;
        bool success = walk(stack[0], buff, &buffindex, bufflen, forceParens);
        if (success)
        {
            if (dynamic)
            {
                free(tempbuff);
            }
            return buff;
        }
    }
    if (dynamic)
    {
        free(tempbuff);
    }
    return null;
}
